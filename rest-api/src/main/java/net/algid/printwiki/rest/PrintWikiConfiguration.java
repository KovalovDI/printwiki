package net.algid.printwiki.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by kovalov on 14.12.16.
 */
@SpringBootApplication
public class PrintWikiConfiguration {

    public static void main(String[] args) {
        SpringApplication.run(PrintWikiConfiguration.class, args);
    }

}
