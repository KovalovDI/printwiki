package net.algid.printwiki.rest.controller;

import net.algid.printwiki.rest.model.Response;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by kovalov on 14.12.16.
 */
@Controller
@RequestMapping("/do")
public class PrintWikiController {

    @RequestMapping(method= RequestMethod.GET)
    public @ResponseBody
    Response sayHello() {
        return new Response(1, "Done!");
    }

}
